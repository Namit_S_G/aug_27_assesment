package com.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

class Player implements Comparable<Player>{
	private int id;
	private String name;
	private int matchesPlayed;
	private int totalRunsScored;
	private int totalWicketsTaken;
	private int outOnZero;
	private String playerType;
	
	public Player() {
		
	}
	
	public Player(int id, String name, int matches_played, int total_runs_scored, int total_wickets_taken,
			int out_on_zero, String player_type) {
		super();
		this.id = id;
		this.name = name.toUpperCase();
		this.matchesPlayed = matches_played;
		this.totalRunsScored = total_runs_scored;
		this.totalWicketsTaken = total_wickets_taken;
		this.outOnZero = out_on_zero;
		this.playerType = player_type.toUpperCase();
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toUpperCase();
	}

	public int getMatchesPlayed() {
		return matchesPlayed;
	}

	public void setMatchesPlayed(int matchesPlayed) {
		this.matchesPlayed = matchesPlayed;
	}

	public int getTotalRunsScored() {
		return totalRunsScored;
	}

	public void setTotalRunsScored(int totalRunsScored) {
		this.totalRunsScored = totalRunsScored;
	}

	public int getTotalWicketsTaken() {
		return totalWicketsTaken;
	}

	public void setTotalWicketsTaken(int totalWicketsTaken) {
		this.totalWicketsTaken = totalWicketsTaken;
	}

	public int getOutOnZero() {
		return outOnZero;
	}

	public void setOutOnZero(int outOnZero) {
		this.outOnZero = outOnZero;
	}

	public String getPlayerType() {
		return playerType;
	}

	public void setPlayerType(String playerType) {
		this.playerType = playerType.toUpperCase();
	}

	
	
	public Player findPlayerByName(List<Player> arr, String PlayerName) {
		Player p1 = new Player();
		PlayerName = PlayerName.toUpperCase();
		
		for(Player pl : arr) {
			if(pl.getName().equals(PlayerName)) {
				return pl;
			}	
		}
		return null;
	}
	
	public int getBowlersCount(List<Player> arr) {
		int bowlerCount = 0;
		for(Player p : arr) {
			if(p.getPlayerType().contentEquals("BOWLER"))
				bowlerCount++;
		}
		return bowlerCount;
	}
	
	public int getWicketKeeperCount(List<Player> arr) {
		int wicketKeeperCount = 0;
		
		for(Player p : arr) {
			if(p.getPlayerType().equalsIgnoreCase("WICKET KEEPER"))
				wicketKeeperCount++;
		}
		return wicketKeeperCount;
	}
	
	public void displayAllPlayers(List<Player> arr) {
		for(Player p : arr) {
			System.out.println(p);
		}
	}
	
	public ArrayList<Player> createTeam(List<Player> arr, int numBowlers, int wicketKeeper) {
		
		ArrayList<Player> final_list = new ArrayList<Player>(11);
		ArrayList<Player> bowlers = new ArrayList<Player>(numBowlers);
		ArrayList<Player> wicketKeepers = new ArrayList<Player>(wicketKeeper);
		ArrayList<Player> others = new ArrayList<Player>(20-numBowlers-wicketKeeper);
		
		for(Player p : arr) {
			if(p.getPlayerType().equalsIgnoreCase("BOWLER")) {
				bowlers.add(p);
			}
			else if(p.getPlayerType().equalsIgnoreCase("WICKET KEEPER")) {
				wicketKeepers.add(p);
			}
			else {
				others.add(p);
			}
		}
		//Sorting according to avg score
		
		Collections.sort(bowlers);
		Collections.sort(wicketKeepers);
		Collections.sort(others);
		
//		System.out.println("Bowlers list raw. list size "+bowlers.size());
//		System.out.println(bowlers);
//		
//		System.out.println("WicketKeeprs list raw. list size"+wicketKeepers.size());
//		System.out.println(wicketKeepers);
//		
//		System.out.println("Others list raw. list size :"+others.size());
//		System.out.println(others);
		
		int teamCount = 0;
		int addedBowlers = 0;
		
		for(Player p : bowlers) {
			if(!final_list.contains(p) && addedBowlers <= numBowlers-1) {
				final_list.add(p);
				addedBowlers++;
				teamCount++;
//				System.out.println("Added player in bowlers"+p.getName());
			}
		}
		
		if(wicketKeeper == 1 && wicketKeepers.size()>0) {
			final_list.add(wicketKeepers.get(0));
			teamCount++;
//			System.out.println("Added player in wicket keepers"+wicketKeepers.get(0).getName());
		}
		
		int remaining_teamCount = 11-teamCount;
		
		for(int i = 0; i < remaining_teamCount; i++) {
			if(!final_list.contains(others.get(i)) && !others.get(i).getPlayerType().equals("BOWLER")) {
				final_list.add(others.get(i));
				teamCount++;
//				System.out.println("Added player in others"+others.get(i).getName());
			}
		}
		
		//Find the top 'numBowlers' from the list to add to the main list
		Collections.sort(final_list);
		
		return final_list;
	}
	public void displayFinalTeam(List<Player> arr) {
		
	}
	
	
	
	@Override
	public String toString() {
		String s = this.getId()+" | "+this.getName()+" | "+this.getMatchesPlayed()+" | "+this.getTotalRunsScored()+" | "+this.getTotalWicketsTaken()+" | "+this.getOutOnZero()+" | "+this.getPlayerType();
		return s;
	}
	public float avg_score() {
		float avg_score = this.getTotalRunsScored()/this.getMatchesPlayed();
		return avg_score;
	}

	@Override
	public int compareTo(Player o) {
		return Float.compare(o.avg_score(), this.avg_score());
	}
	
}
public class Exp1 {
	public static void main(String[] args) throws PlayerNotFoundException{
		
		Scanner sc = new Scanner(System.in);
		int ch = 0;
		
		//Creating a player class object
		Player p = new Player();
				
		//Creating an arraylist of players
		ArrayList<Player> li = new ArrayList<Player>(20);
		
//		li.add(new Player(1, "Virat", 2000, 15000, 165, 600, "Batsman"));
//		li.add(new Player(2, "Dhoni", 3000, 14000, 100, 300, "Batsman"));
//		li.add(new Player(3, "Ashwin", 1500, 6541, 650, 500, "Bowler"));
//		
//		li.add(new Player(4, "Arvind", 1052, 3201, 225, 400, "Batsman"));
//		li.add(new Player(5, "Venky", 1346, 10152, 400, 350, "Batsman"));
//		li.add(new Player(6, "Prasad", 1500, 6352, 850, 500, "Bowler"));
//		
//		li.add(new Player(7, "Rabada", 1352, 6954, 865, 700, "Bowler"));
//		li.add(new Player(8, "Stark", 3000, 14532, 552, 300, "Bowler"));
//		li.add(new Player(9, "Steyn", 1500, 6475, 655, 500, "Bowler"));
//		
//		li.add(new Player(10, "Rohit", 2000, 14965, 165, 500, "Batsman"));
//		li.add(new Player(11, "Rahul", 1500, 13596, 102, 306, "Batsman"));
//		li.add(new Player(12, "Chahal", 1500, 12546, 898, 500, "Allrounder"));
//		
//		li.add(new Player(13, "Anushka", 2000, 11234, 165, 210, "Batsman"));
//		li.add(new Player(14, "Deepika", 1100, 6641, 100, 321, "Batsman"));
//		li.add(new Player(15, "Ranveer", 3000, 9564, 1264, 412, "Allrounder"));
//		
//		li.add(new Player(16, "Varun", 2000, 12535, 98, 850, "Wicket Keeper"));
//		li.add(new Player(17, "Tiger", 1653, 12354, 150, 352, "Batsman"));
//		li.add(new Player(18, "Amitabh", 1234, 7456, 786, 652, "Allrounder"));
//		
//		li.add(new Player(19, "Rajkumar", 2000, 22000, 186, 300, "Batsman"));
//		li.add(new Player(20, "Akshay", 3000, 11234, 105, 895, "Batsman"));
		
		System.out.println("==============================================================================================");
		while(true) 
		{
		System.out.println("*****************MENU*****************");
		System.out.println("1. Display All players");
		System.out.println("2. Update player information by name");
		System.out.println("3. Display Final Team");
		System.out.println("4. Add Player Information");
		System.out.println("5. Exit");
		System.out.println();
		System.out.println("MAKE YOUR CHOICE : ");
		
		//Accepting the input from the user
		ch = sc.nextInt();
		
		
		// name, matches_played, total_runs, wickets_taken, out_on_zero, player_type
		
		
		
		
		Collections.sort(li);
		
		switch(ch)
		{
		case 1: if(li.size() == 0) {
					System.out.println("NO PLAYERS IN THE LIST. PLEASE ADD PLAYERS IN THE LIST TO UPDATE.");
					break;
				}
				p.displayAllPlayers(li);
				break;
				
		case 2: if(li.size() == 0) {
					System.out.println("NO PLAYERS IN THE LIST. PLEASE ADD PLAYERS IN THE LIST TO UPDATE.");
					break;
				}
				System.out.println("Enter the name of the player to update information : ");
		        String playerName = sc.next();
		        Player res = p.findPlayerByName(li, playerName);
		        if(res != null) {
		        	System.out.println("The stored information about the player is : ");
        			System.out.println(res);
		        	System.out.println("Enter the information to update");
		        	System.out.println("1. Name\n2. Matches Played\n3.Total runs scored\n4. Total Wickets Taken\n5. Out on zero\n");
		        	System.out.println("Enter your choice : ");
		        	
		        	int field = sc.nextInt();
		        	
		        	switch(field) {
		        	
		        	case 1: 
		        			
		        			System.out.println("Enter the new name : ");
		        	        String new_name = sc.next();
		        	        res.setName(new_name);
		        	        
		        	        System.out.println("Updated values are : ");
        	        		System.out.println(res);
		        	        break;
		        	        
		        	case 2: 
        					
		        			System.out.println("Enter the new matches played : ");
        	        		int new_matches_played = sc.nextInt();
        	        		res.setMatchesPlayed(new_matches_played);
        	        		
        	        		System.out.println("Updated values are : ");
        	        		System.out.println(res);
        	        		break;
        	        		
		        	case 3: 
		        		
		        			System.out.println("Enter the new total runs scored : ");
        	        		int new_total_runs_scored = sc.nextInt();
        	        		res.setTotalRunsScored(new_total_runs_scored);
        	        		System.out.println("Updated values are : ");
        	        		System.out.println(res);
        	        		break;
        	        		
		        	case 4: 
		        			
		        			System.out.println("Enter the new total wickets taken : ");
        	        		int new_wickets_taken = sc.nextInt();
        	        		res.setTotalWicketsTaken(new_wickets_taken);
        	        		
        	        		System.out.println("Updated values are : ");
        	        		System.out.println(res);
        	        		break;
        	        		
		        	case 5: 
		        		
		        			System.out.println("Enter the new total outs on zero : ");
		        			int new_out_on_zero = sc.nextInt();
		        			res.setOutOnZero(new_out_on_zero);
		        			
		        			System.out.println("Updated values are : ");
        	        		System.out.println(res);
		        			break;
		        			
//		        	case 6: System.out.println("The stored information about the player is : ");
//        					System.out.println(res);
//		        		
//		        			System.out.println("Enter the new player type : ");
//        	        		String new_player_type = sc.next();
//        	        		new_player_type = new_player_type.toUpperCase();
//        	        		while(!new_player_type.equalsIgnoreCase("Batsman") || !new_player_type.equalsIgnoreCase("Bowler") || !new_player_type.equalsIgnoreCase("Allrounder") || !new_player_type.equalsIgnoreCase("Wicket Keeper")){
//        	        			System.out.println("UPDATE DENIED! Enter a valid player type.");
//        	        			System.out.println("Please enter Batsman OR Bowler OR AllRounder OR Wicket Keeper");
//        	        			new_player_type = sc.next();
//        	        		}
//        	        		res.setPlayerType(new_player_type);
//        	        		
//        	        		System.out.println("Updated values are : ");
//        	        		System.out.println(res);
//        	        		break;
        	        		
        	        default: System.out.println("Wrong choice");
		        	}
		        	
		     
		        }
		        else {
		        	try {
		        		PlayerNotFoundException pnf = new PlayerNotFoundException("PLAYER "+playerName+" NOT FOUND EXCEPTION!");
		        		throw(pnf);
		        	}
		        	catch(PlayerNotFoundException pnf) {
		        		//handling the playernotfound exception
		        		System.out.println("PLAYER "+playerName+" NOT FOUND!! PLEASE ENTER A VALID PLAYERNAME.");
		        	}
		        }
		        break;
		        
		
		case 3: if(li.size() == 0) {
					System.out.println("THE LIST IS EMPTY! PLEASE ENTER PLAYERS IN THE LIST TO DISPLAY A FINAL LIST!");
					break;
				}
				System.out.println("Displaying final team"); 
				System.out.println("Enter the total number of bowlers required to Display the team.");
				
				int numBowlers = sc.nextInt();
				int bowlersInTeam = p.getBowlersCount(li);
				
				if(numBowlers < 3) {
					System.out.println("Enter atleast 3 bowlers. The entered number of bowlers is "+numBowlers);
					break;
				}
				while(numBowlers > bowlersInTeam ) {
					
					System.out.println("The total number of available bowlers in the 20 entered players is : "+bowlersInTeam);
					System.out.println("The total number of bowlers required in the team is : "+numBowlers);
					System.out.println("The minimum number of bowlers required in the team is : 3");
					System.out.println("Please enter the number of bowlers less than or equal to number of available bowlers and a number greater than or equal to 3");
					numBowlers = sc.nextInt();
				}
				if(numBowlers < 3) {
					break;
				}
				
				System.out.println("Should the wicketkeeper be included in the team?\nPRESS 1 FOR YES\nPRESS 0 FOR NO");
				int wicketKeeper = sc.nextInt();
				while(wicketKeeper != 1 && wicketKeeper != 0) {
					System.out.println("Please enter a valid choice.");
					wicketKeeper = sc.nextInt();
				}
				
				int wicketKeepersInTeam = p.getWicketKeeperCount(li);
				if(wicketKeepersInTeam < 1) {
					System.out.println("There are no wicketkeepers in the entered 20.\nConsidering a team without wicketkeepers.");
				}
				
				ArrayList<Player> final_list = new ArrayList<Player>(11);
				final_list = p.createTeam(li, numBowlers, wicketKeeper);
				
				System.out.println("The final list is : ");
				System.out.println("ID | NAME | M. PLAYED | RUNS | WICKETS | OUT ON ZERO | PLAYER TYPE");
				for(Player pla : final_list) {
					System.out.println(pla);
				}
				break;
				
		case 4: if(li.size()==20) {
					System.out.println("THE GIVEN LIST ALREADY CONTAINS 20 PLAYERS. CANNOT ADD MORE PLAYERS!!");
					break;
				}
				
				int enteredBowlers = 0, enteredWicketKeepers= 0, enteredBatsmen = 0, enteredAllrounders = 0, totalNumber = 1;
				System.out.println("Enter the number of bowlers : ");
				while(enteredBowlers < 3 || enteredBowlers > 20) {
					System.out.println("The minimum number of bowlers is 3 and maximum is 20");
					System.out.println("Please enter the number of bowlers within the range 3-20");
					enteredBowlers = sc.nextInt();
					
				}
				
				System.out.println("======================================================================================");
				System.out.println("Enter the details of the player : ");
								
				for(int i = 0; i < enteredBowlers; i++) {
					System.out.println("Enter Player["+(totalNumber)+"]'s details : ");
					
					System.out.println("NAME : ");
					String name = sc.next();
					name = name.toUpperCase();
					if(p.findPlayerByName(li, name) != null) {
						while(p.findPlayerByName(li, name) != null) {
							System.out.println("The player "+name+" already exists. Please enter another name.");
							name = sc.next();
						}
					}
					
					System.out.println("MATCHES PLAYED :");
					int numMatches = sc.nextInt();
					
					System.out.println("TOTAL RUNS SCORED : ");
					int totalRuns = sc.nextInt();
					
					System.out.println("TOTAL WICKETS TAKEN :");
					int totalWickets = sc.nextInt();
					
					System.out.println("OUT ON ZERO : ");
					int outOnZero = sc.nextInt();
					
					
					System.out.println("PLAYER TYPE");
					System.out.println("BOWLER");
					
					
					Player newPlayer = new Player(totalNumber, name, numMatches, totalRuns, totalWickets, outOnZero, "BOWLER");
					li.add(newPlayer);
					
					System.out.println("NEW PLAYER SUCCESSFULLY ADDED TO THE LIST!");
					System.out.println(newPlayer);
					System.out.println("----------------------------------------------------------------------------------------------------------");
					totalNumber++;
				}

				if(totalNumber <= 20) {
					sc.nextLine();
					System.out.println("Enter the number of Wicket Keepers (0 or 1): ");
					
					enteredWicketKeepers = sc.nextInt();
					while(enteredWicketKeepers > 1 || enteredWicketKeepers < 0) {
						System.out.println("The wicket keepers entered is : "+enteredWicketKeepers);
						System.out.println("The maximum allowed Wicket Keepers in the team is : 1");
						System.out.println("Please enter the number of batsmen within the range 0-1");
						enteredWicketKeepers = sc.nextInt();
					}
					System.out.println("======================================================================================");
					System.out.println("Enter the details of the player : ");
									
					for(int i = 0; i < enteredWicketKeepers; i++) {
						System.out.println("Enter Player["+(totalNumber)+"]'s details : ");
						
						System.out.println("NAME : ");
						String name = sc.next();
						name = name.toUpperCase();
						if(p.findPlayerByName(li, name) != null) {
							while(p.findPlayerByName(li, name) != null) {
								System.out.println("The player "+name+" already exists. Please enter another name.");
								name = sc.next();
							}
						}
						
						System.out.println("MATCHES PLAYED :");
						int numMatches = sc.nextInt();
						
						System.out.println("TOTAL RUNS SCORED : ");
						int totalRuns = sc.nextInt();
						
						System.out.println("TOTAL WICKETS TAKEN :");
						int totalWickets = sc.nextInt();
						
						System.out.println("OUT ON ZERO : ");
						int outOnZero = sc.nextInt();
						
						
						System.out.println("PLAYER TYPE");
						System.out.println("WICKET KEEPER");
						
						
						Player newPlayer = new Player(totalNumber, name, numMatches, totalRuns, totalWickets, outOnZero, "WICKET KEEPER");
						li.add(newPlayer);
						
						System.out.println("NEW PLAYER SUCCESSFULLY ADDED TO THE LIST!");
						System.out.println(newPlayer);
						System.out.println("----------------------------------------------------------------------------------------------------------");
						totalNumber++;
					}
				}
				
				
				if(totalNumber <= 20) {
					System.out.println("Enter the number of Batsmen : ");
					enteredBatsmen = sc.nextInt();
					while(enteredBatsmen > 20 - enteredBowlers - enteredWicketKeepers || enteredBatsmen < 0) {
						System.out.println("The bowlers entered is : "+enteredBowlers);
						System.out.println("The wicket keepers entered is : "+enteredWicketKeepers);
						System.out.println("The remaining number of players to enter is 20 - "+(enteredBowlers + enteredWicketKeepers)+" = "+(20-enteredBowlers-enteredWicketKeepers));
						System.out.println("Please enter the number of batsmen within the range 0-"+(20-enteredBowlers-enteredWicketKeepers));
						enteredBatsmen = sc.nextInt();
					}
					
					System.out.println("======================================================================================");
					System.out.println("Enter the details of the player : ");
					for(int i = 0; i < enteredBatsmen; i++) {
						System.out.println("Enter Player["+(totalNumber)+"]'s details : ");
						
						System.out.println("NAME : ");
						String name = sc.next();
						name = name.toUpperCase();
						
						if(p.findPlayerByName(li, name) != null) {
							while(p.findPlayerByName(li, name) != null) {
								System.out.println("The player "+name+" already exists. Please enter another name.");
								name = sc.next();
							}
						}
						
						System.out.println("MATCHES PLAYED :");
						int numMatches = sc.nextInt();
						
						System.out.println("TOTAL RUNS SCORED : ");
						int totalRuns = sc.nextInt();
						
						System.out.println("TOTAL WICKETS TAKEN :");
						int totalWickets = sc.nextInt();
						
						System.out.println("OUT ON ZERO : ");
						int outOnZero = sc.nextInt();
						
						
						System.out.println("PLAYER TYPE");
						System.out.println("BATSMAN");
						
						
						Player newPlayer = new Player(totalNumber, name, numMatches, totalRuns, totalWickets, outOnZero, "BATSMAN");
						li.add(newPlayer);
						
						System.out.println("NEW PLAYER SUCCESSFULLY ADDED TO THE LIST!");
						System.out.println(newPlayer);
						System.out.println("----------------------------------------------------------------------------------------------------------");
						totalNumber++;
					}
				}
				
				
				if(totalNumber <= 20) {
					System.out.println("Enter the number of allrounders : ");
					enteredAllrounders = sc.nextInt();
					while(enteredAllrounders != 20 - enteredBowlers - enteredWicketKeepers - enteredBatsmen|| enteredAllrounders < 0) {
						System.out.println("The bowlers entered is : "+enteredBowlers);
						System.out.println("The wicket keepers entered is : "+enteredWicketKeepers);
						System.out.println("The batsmen entered is : "+enteredBatsmen);
						System.out.println("The remaining number of players to enter is 20 - "+(enteredBowlers + enteredWicketKeepers + enteredBatsmen)+" = "+(20-enteredBowlers-enteredWicketKeepers - enteredBatsmen));
						System.out.println("Please enter the details of "+(20-enteredBowlers-enteredWicketKeepers - enteredBatsmen)+" allrounders. Press "+(20-enteredBowlers-enteredWicketKeepers - enteredBatsmen)+" to continue");
						enteredAllrounders = sc.nextInt();
					}
					
					System.out.println("======================================================================================");
					System.out.println("Enter the details of the player : ");
					for(int i = 0; i < enteredAllrounders; i++) {
						System.out.println("Enter Player["+(totalNumber)+"]'s details : ");
						
						System.out.println("NAME : ");
						String name = sc.next();
						name = name.toUpperCase();
						if(p.findPlayerByName(li, name) != null) {
							while(p.findPlayerByName(li, name) != null) {
								System.out.println("The player "+name+" already exists. Please enter another name.");
								name = sc.next();
							}
						}
						
						System.out.println("MATCHES PLAYED :");
						int numMatches = sc.nextInt();
						
						System.out.println("TOTAL RUNS SCORED : ");
						int totalRuns = sc.nextInt();
						
						System.out.println("TOTAL WICKETS TAKEN :");
						int totalWickets = sc.nextInt();
						
						System.out.println("OUT ON ZERO : ");
						int outOnZero = sc.nextInt();
						
						
						System.out.println("PLAYER TYPE");
						System.out.println("ALLROUNDER");
						
						
						Player newPlayer = new Player(totalNumber, name, numMatches, totalRuns, totalWickets, outOnZero, "ALLROUNDER");
						li.add(newPlayer);
						
						System.out.println("NEW PLAYER SUCCESSFULLY ADDED TO THE LIST!");
						System.out.println(newPlayer);
						System.out.println("----------------------------------------------------------------------------------------------------------");
						totalNumber++;
					}
				
				}
				System.out.println("==============================================================================================================");
				System.out.println("The TEAM OF 20 PLAYERS INSERTED IS : ");
				p.displayAllPlayers(li);
				System.out.println("The size of the team is "+li.size());
				System.out.println("==============================================================================================================");
				break;
				
				
		case 5: System.out.println("THANK YOU. EXITING...");
				System.exit(0);
				break;
		
		default: System.out.println("WRONG CHOICE ENTERED!! PLEASE ENTER A NUMBER BETWEEN 1-5.");
				 break;
				
		}
		
		}
	}
}
